angular.module('starter.controllers')
  .controller('LoginCtrl', function($scope, $rootScope, $ionicHistory, UserService, Utils) {
    $scope.loginCtrl = this
    this.user = ''
    this.pwd = ''
    this.login = function() {
      UserService.login({
        number: this.num,
        pwd: this.pwd
      }).$promise.then(function(res) {
        if(Utils.isResponseOk(res)) {
          Utils.setUser(res)
          $ionicHistory.clearCache()
          $rootScope.goBack()
        }
      })
    }
  })
