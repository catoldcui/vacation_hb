angular.module('starter.services')

.factory('UserService', function($resource) {
  return $resource(baseUrl + '/user/:r1/:r1Id', {}, {
    login: {
      method: 'GET',
      params: {
        r1: 'login'
      }
    },
    logout: {
      method: 'GET',
      params: {
        r1: 'logout'
      }
    },
    // 设置密码
    setpwd: {
      method: 'PUT',
      params: {
        r1: 'pwd',
        oldpwd: '@oldpwd',
        newpwd: '@newpwd'
      },
    },
    // 设置个人信息，{user_id,phone,email}
    setinfo: {
      method: 'PUT',
      params: {
        r1: 'info',
        phone: '@phone',
        email: '@email'
      }
    },
    // 设置头像
    setavator: {
      method: 'PUT',
      params: {
        r1: 'avator',
        r1Id: '@r1Id'
      }
    },
    // 获取个人绩效 /evaluation?fromyear=&frommonth=&toyear=&tomonth=
    getevaluation: {
      method: 'GET',
      params: {
        r1: 'evaluation',
        fromyear: '@fromyear',
        toyear: '@toyear',
        frommonth: '@frommonth',
        tomonth: '@tomonth'
      }
    },
    // 获取需要该用户审核的信息 /own_vacation?page=&num=
    getownvacation: {
      method: 'GET',
      isArray: true,
      params: {
        r1: 'own_vacation'
      }
    },
    // 统计部获取统计信息， /amount?depart_id=&year=&*month=&email=
    getamount: {
      method: 'GET',
      params: {
        r1: 'amount',
        fromyear: '@fromyear',
        toyear: '@toyear',
        frommonth: '@frommonth',
        tomonth: '@tomonth',
        email: '@email',
        depart_id: '@depart_id'
      }
    },
    noop: {
      method: 'OPTIONS',
      params: {
        r1: 'noop'
      }
    },
    getmarks: {
      method: 'GET',
      isArray: true,
      params: {
        r1: 'mark'
      }
    },
  })
})
