appCtrl = angular.module('starter.controllers', [])

appCtrl
// .controller('MineCtrl', function($scope) {})


  .controller('MineCtrl', function($scope) {})
  .controller('MineDashCtrl', function($scope, $ionicPopup, $rootScope, $log, Utils, UserService) {
    $scope.mineDashCtrl = this
    var _this = this
    this.changePwd = function() {
      _this.data = {}
      var inputHtml = '<div class="list"><label class="item item-input item-floating-label"><span class="input-label">旧密码</span><input placeholder="旧密码" type="password" ng-model="mineDashCtrl.data.oldpwd"></label><label class="item item-input item-floating-label"><span class="input-label">新密码</span><input placeholder="请输入新密码" type="password" ng-model="mineDashCtrl.data.newpwd"></label><label class="item item-input item-floating-label"><span class="input-label">确认密码</span><input placeholder="确认密码" type="password" ng-model="mineDashCtrl.data.confirmpwd"></label></div>'
        // An elaborate, custom popup
      var myPopup = $ionicPopup.show({
        template: inputHtml,
        title: '修改密码',
        scope: $scope,
        buttons: [{
          text: '取消'
        }, {
          text: '<b>确定</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (_this.data.oldpwd && _this.data.newpwd && _this.data.newpwd.length > 0 && _this.data.confirmpwd == _this.data.newpwd) {
              return _this.data
            } else {
              $rootScope.toast('请正确填写信息')
              e.preventDefault()
            }
          }
        }]
      })
      myPopup.then(function(val) {
        if (!val) return
        $log.info(val)
        UserService.setpwd({
          oldpwd: val.oldpwd,
          newpwd: val.newpwd
        }).$promise.then(function(res) {
          if (Utils.isResponseOk(res)) {
            $rootScope.toast('修改密码成功')
          }
        })
      })
    }

    this.logout = function() {
      UserService.logout().$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          Utils.removeUser()
          $rootScope.toast('注销成功')
        }
      })
    }



  })
  .controller('MineInfoCtrl', function($scope, $ionicPopup, $rootScope, UserService, Utils) {
    $scope.mineInfoCtrl = this
    var _this = this
    this.changePhone = function() {
      $ionicPopup.prompt({
        title: '修改手机号',
        template: ' ',
        inputType: 'phone',
        inputPlaceholder: '新手机号'
      }).then(function(res) {
        console.log('Your phone is', res)
        if (!res) return

        UserService.setinfo({
          phone: res
        }).$promise.then(function(res) {
          if (Utils.isResponseOk(res)) {
            $rootScope.toast('修改手机号成功')
            $scope.$emit('user:update')
          }
        })
      })
    }

    this.changeEmail = function() {
      $ionicPopup.prompt({
        title: '修改Email',
        template: ' ',
        inputType: 'email',
        inputPlaceholder: 'email地址'
      }).then(function(res) {
        console.log('Your email is', res)
        if (!res) {
          return
        }
        UserService.setinfo({
          email: res
        }).$promise.then(function(res) {
          if (Utils.isResponseOk(res)) {
            $rootScope.toast('修改邮箱成功')
            $scope.$emit('user:update')
          }
        })
      })
    }

    this.upload = function(url) {
      console.log("开始上传：")
      var wt = plus.nativeUI.showWaiting()
      var task = plus.uploader.createUpload(baseUrl + '/file', {
          method: "POST"
        },
        function(t, status) { //上传完成
          console.log(status)
          console.log(angular.toJson(t))
          wt.close()
          if (status == 200) {
            console.log("上传成功：" + t.responseText)
            var response = angular.fromJson(t.responseText)
            if (Utils.isResponseOk(response)) {
              console.log(response[0])
              UserService.setavator({
                r1Id: response[0]
              }).$promise.then(function(res) {
                if (Utils.isResponseOk(res)) {
                  $rootScope.toast('修改头像成功')
                  $scope.$emit('user:update')
                }
              })
            } else {
              $rootScope.toast('提交失败')
            }
          }
        }
      )

      task.addFile(url, {
        key: 'file'
      })
      task.start()
    }

    _this.changeHead = function() {
      //  	outSet( "弹出系统选择按钮框：" )
      // alert(1)
      var bts = [{
        title: "拍照上传"
      }, {
        title: "相册中选择"
      }]
      plus.nativeUI.actionSheet({
          title: "修改头像",
          cancel: "取消",
          buttons: bts
        },

        function(e) {
          switch (e.index) {
            case 1:
              // 拍照上传
              console.log("开始拍照：")
              var cmr = plus.camera.getCamera()
              cmr.captureImage(function(p) {
                console.log("成功：" + p)
                plus.io.resolveLocalFileSystemURL(p, function(entry) {
                  _this.upload(entry.toLocalURL())
                }, function(e) {
                  console.log("读取拍照文件错误：" + e.message)
                })
              }, function(e) {
                console.log("失败：" + e.message)
              }, {
                filename: "_doc/camera/",
                index: 1
              })
              break
            case 2:
              // 系统相册
              plus.gallery.pick(function(path) {
                plus.io.resolveLocalFileSystemURL(path, function(entry) {
                  _this.upload(entry.toLocalURL())
                }, function(e) {
                  console.log("读取文件错误：" + e.message)
                })
              }, function(e) {
                console.log("取消选择图片")
              }, {
                filter: "image"
              })
              break
          }
        }
      )
    }
  })
  .controller('MineEvaluateCtrl', function($scope, $rootScope, Utils, UserService) {
    // 0 本月  1：今年  2：上月
    $scope.mineEvaluateCtrl = this
    var _this = this

    _this.years = [2016, 2015, 2014]
    _this.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    _this.fromyear = 2015
    _this.frommonth = 1
    _this.toyear = 2016
    _this.tomonth = 12
    _this.doRefresh = function() {
        UserService.getevaluation({
          fromyear: _this.fromyear,
          toyear: _this.toyear,
          frommonth: _this.frommonth,
          tomonth: _this.tomonth
        }).$promise.then(function(res) {
          if (Utils.isResponseOk(res)) {
            _this.eva = res
            _this.leaves = []
            for (var i = 0; i < res.leaveHours.length; i++) {
              var leave = {
                name: $rootScope.findFirstType(i + 1).name,
                time: res.leaveHours[i]
              }
              _this.leaves.push(leave)
            }
          }
        })
      }
      // _this.doRefresh()
  })
  .controller('MineAmountCtrl', function($scope, $rootScope, Utils, DepartService, UserService) {
    $scope.mineAmountCtrl = this
    var _this = this
    _this.years = [2016, 2015, 2014]
    _this.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    _this.fromyear = 2015
    _this.frommonth = 1
    _this.toyear = 2016
    _this.tomonth = 12

    this.doRefresh = function() {
      DepartService.query().$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.departs = res
        }
        $scope.$broadcast('scroll.refreshComplete')
      })
    }

    this.getAmount = function() {
      UserService.getamount({
        fromyear: _this.fromyear,
        toyear: _this.toyear,
        frommonth: _this.frommonth,
        tomonth: _this.tomonth,
        depart_id: _this.depart,
        email: _this.email
      }).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          $rootScope.toast('操作成功，请通知查收邮件')
        }
      })
    }
    _this.doRefresh()
  })
  .controller('MineMarkCtrl', function($scope, UserService, Utils) {
    $scope.mineMarkCtrl = this
    var _this = this
    _this.doRefresh = function() {
      UserService.getmarks().$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.marks = res
        }
        $scope.$broadcast('scroll.refreshComplete')
      })
    }

    _this.doRefresh()
  })
