angular.module('starter.services')

.factory('SystemService', function($resource) {
  return $resource(baseUrl + '/system/:r1', {}, {
    getNotification: {
      method: 'GET',
      params: {
        'r1': 'notification'
      }
    },
    // formdata: content
    setNotification: {
      method: 'PUT',
      params: {
        'r1': 'notification'
      }
    },

  })
})
