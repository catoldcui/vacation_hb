angular.module('starter.filter', [])
  .filter('vacationStatusZh', ['$rootScope', function($rootScope) {
    // WAITING_FOR_FIRST_APPLY("等待审批"),
    // FIRST_PASSED("一级审批通过", VacationStatus.WAITING_FOR_FIRST_APPLY),
    // FIRST_BLOCKED("一级审批驳回", VacationStatus.WAITING_FOR_FIRST_APPLY),
    // SECOND_PASSED("二级审批通过", VacationStatus.FIRST_PASSED),
    // SECOND_BLOCKED("二级审批驳回", VacationStatus.FIRST_PASSED),
    // WAITING_FOR_DELAY_APPLY("延期等待审批", VacationStatus.SECOND_PASSED, VacationStatus.DELAY_BLOCKED, VacationStatus.DELAY_PASSED),
    // DELAY_BLOCKED("延期未通过", VacationStatus.WAITING_FOR_DELAY_APPLY),
    // DELAY_PASSED("延期通过", VacationStatus.WAITING_FOR_FIRST_APPLY)
    function decorateFilter(input) {
      var result = '状态错误'
      switch (input) {
        case 'WAITING_FOR_FIRST_APPLY':
          result = '等待审批'
          break;
        case 'FIRST_PASSED':
          result = '上级审批通过'
          break;
        case 'FIRST_BLOCKED':
          result = '上级驳回'
          break;
        case 'SECOND_PASSED':
          result = '统计部通过'
          break;
        case 'SECOND_BLOCKED':
          result = '统计部驳回'
          break;
        case 'WAITING_FOR_DELAY_APPLY':
          result = '延期等待审批'
          break;
        case 'DELAY_PASSED':
          result = '延期通过'
          break;
        case 'DELAY_BLOCKED':
          result = '延期未通过'
          break;
        default:
      }

      return result
    }
    decorateFilter.$stateful = true

    return decorateFilter
  }])
