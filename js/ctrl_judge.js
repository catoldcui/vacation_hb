appCtrl
// .controller('MineCtrl', function($scope) {})

  .controller('JudgeListCtrl', function($scope, Utils, UserService) {
    $scope.judgeListCtrl = this
    var _this = this

    this.doRefresh = function() {
      UserService.getownvacation().$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.judgeList = res
        }
        $scope.$broadcast('scroll.refreshComplete')

      })
    }

    this.doRefresh()
  })
  .controller('JudgeDetailCtrl', function($scope, $rootScope, $state, $stateParams, $ionicPopup, Utils, VacationService) {
    $scope.judgeDetailCtrl = this
    var _this = this

    _this.doRefresh = function() {
      VacationService.get({
        vacationId: $stateParams.id
      }).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.vacation = res
        }
        $scope.$broadcast('scroll.refreshComplete')
      })
    }

    _this.setApplyState = function(type) {
      _this.note = {
        content: ''
      }
      var title = ''
      var template = ''
      var buttons = [{
        text: '取消'
      }, {
        text: '<b>确定</b>',
        type: 'button-positive'
      }]
      if (type) {
        // tode 设置通过
        title = "通过"
        template = "是否通过审批？"
        buttons[1].onTap = function(e) {
          return true
        }
      } else {
        title = "驳回"
        template = "<input type='text' ng-model='judgeDetailCtrl.note.content'>"
        buttons[1].onTap = function(e) {
          return _this.note.content
        }
      }

      // An elaborate, custom popup
      var myPopup = $ionicPopup.show({
        template: template,
        title: title,
        scope: $scope,
        buttons: buttons
      })
      myPopup.then(function(res) {
        console.log('Tapped!', res)
        // todo 更新状态
        if (res || res == '') {
          // $rootScope.goBack()
          var status = ""
          if(Utils.isStatistic()){
            status = type? 'SECOND_PASSED' : 'SECOND_BLOCKED'
          } else {
            status = type? 'FIRST_PASSED' : 'FIRSTBLOCKED'
          }

          VacationService.updateStatus({
            vacationId: $stateParams.id,
            r1Id: status
          }).$promise.then(function(res){
            if(Utils.isResponseOk(res)){
              $rootScope.toast('审批提交成功')
              _this.vacation.vacationInfo.status = status
            }
          })
        }
      })
    }

    _this.goVacationDelay = function(vacation) {
      if (!vacation.leaveDelayId) {
        return
      }

      $state.transitionTo('tab.judge.delaydetail', {
        id: $stateParams.id,
        delay_id: vacation.leaveDelayId
      })
      // body...
    }
    _this.doRefresh()
  })
  .controller('JudgeDelayDetailCtrl', function($scope, $stateParams, $rootScope, $state, $ionicPopup, VacationService, Utils) {
    $scope.judgeDelayDetailCtrl = this
    var _this = this

    _this.doRefresh = function(){
      VacationService.get({
        vacationId: $stateParams.id
      }).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.vacation = res
          _this.setSubType()

        }
        $scope.$broadcast('scroll.refreshComplete')
      })
      VacationService.getDelayDetail({
        vacationId: $stateParams.id,
        r1Id: $stateParams.delay_id
      }).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.vacationDelay = res
          _this.setSubType()
        }
        $scope.$broadcast('scroll.refreshComplete')

      })
    }

    _this.setSubType = function(){
      if(!_this.vacation || !_this.vacationDelay) return
      _this.subtypelist = $rootScope.generateSubTypes(_this.vacation.vacationInfo.type, _this.vacationDelay.type)
      console.log(_this.subtypelist)
    }

    _this.setApplyState = function(type) {
      _this.note = {
        content: ''
      }
      var title = ''
      var template = ''
      var buttons = [{
        text: '取消'
      }, {
        text: '<b>确定</b>',
        type: 'button-positive'
      }]
      if (type) {
        // tode 设置通过
        title = "通过"
        template = "是否通过审批？"
        buttons[1].onTap = function(e) {
          return true
        }
      } else {
        title = "驳回"
        template = "<input type='text' ng-model='judgeDelayDetailCtrl.note.content'>"
        buttons[1].onTap = function(e) {
          return _this.note.content
        }
      }

      // An elaborate, custom popup
      var myPopup = $ionicPopup.show({
        template: template,
        title: title,
        scope: $scope,
        buttons: buttons
      })
      myPopup.then(function(res) {
        console.log('Tapped!', res)
        // todo 更新状态
        if (res || res == '') {
          // $rootScope.goBack()
          var status = ""
          if(Utils.isStatistic()){
            status = type? 'SECOND_PASSED' : 'SECOND_BLOCKED'
          } else {
            status = type? 'FIRST_PASSED' : 'FIRSTBLOCKED'
          }

          VacationService.updateDelayStatus({
            vacationId: $stateParams.id,
            r1Id: $stateParams.delay_id,
            r2Id: status
          }).$promise.then(function(res){
            if(Utils.isResponseOk(res)){
              $rootScope.toast('审批提交成功')
              _this.vacationDelay.status = status
            }
          })
        }
      })
    }

    _this.doRefresh()
  })
