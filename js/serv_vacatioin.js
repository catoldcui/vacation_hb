angular.module('starter.services')

.factory('VacationService', function($resource) {
  return $resource(baseUrl + '/vacation/:vacationId/:r1/:r1Id/:r2/:r2Id', {
    'vacationId': '@id'
  }, {
    // 获取延期详情
    getDelayDetail: {
      method: 'GET',
      params: {
        r1: 'delay'
      }
    },
    // 申请延期
    applyDelay: {
      method: 'POST',
      params: {
        r1: 'delay'
      }
    },
    // 设置状态
    updateStatus: {
      method: 'PUT',
      params: {
        r1: 'status',
        vacationId: '@vacationId',
        r1Id: '@r1Id'
      }
    },
    // 设置延期状态
    updateDelayStatus: {
      method: 'PUT',
      params: {
        vacationId: '@vacationId',
        r1Id: '@r1Id',
        r1: 'delay',
        r2: 'status',
        r2Id: '@r2Id'
      }
    },

  })
})
