appCtrl
// .controller('MineCtrl', function($scope) {})

.controller('NotiCtrl', function($scope, Utils, VacationViewService) {
  $scope.notiCtrl = this
  var _this = this

  this.startdate = new Date((parseInt((new Date()).getTime() / 3600 / 1000 / 24) * 24 - 8) * 3600 * 1000)
  _this.enddate = new Date(_this.startdate.getTime() + 3600 * 24 * 1000)

  _this.datePickerCallback = function (val) {
    if(!val) return
    _this.datepickerObject.inputDate = val
    _this.startdate = val
    _this.enddate = new Date(_this.startdate.getTime() + 3600 * 24 * 1000)

    _this.doRefresh()
  }

  _this.datepickerObject = {
      titleLabel: '请选择日期',  //Optional
      todayLabel: '今天',  //Optional
      closeLabel: '关闭',  //Optional
      setLabel: '选中',  //Optional
      setButtonType : 'button-positive',  //Optional
      todayButtonType : 'button-light',  //Optional
      closeButtonType : 'button-assertive',  //Optional
      inputDate: _this.startdate,  //Optional
      mondayFirst: true,  //Optional
      // disabledDates: disabledDates, //Optional
      weekDaysList: Utils.weekDaysListZh, //Optional
      monthList: Utils.monthListZh, //Optional
      templateType: 'modal', //Optional
      showTodayButton: 'false', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      // from: new Date(2012, 8, 2), //Optional
      // to: new Date(2018, 8, 25),  //Optional
      callback: function (val) {  //Mandatory
        _this.datePickerCallback(val)
      },
      dateFormat: 'yyyy-MM-dd', //Optional
      closeOnSelect: true, //Optional
    }

    _this.doRefresh = function(){
      VacationViewService.query({
        starttime: _this.startdate.getTime(),
        endtime: _this.enddate.getTime()
      }).$promise.then(function(res){
        if(Utils.isResponseOk(res)){
          _this.views = res
        }
        $scope.$broadcast('scroll.refreshComplete')
      })
    }
    _this.doRefresh()
})
