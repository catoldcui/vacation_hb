appCtrl
// .controller('MineCtrl', function(this) {})

  .controller('VacationsCtrl', function($scope) {})
  .controller('VacationsDetailCtrl', function($scope, $state, $stateParams, VacationService, Utils, StatusUtil) {
    $scope.vacationsDetailCtrl = this
    var _this = this
    this.statusUtil = StatusUtil

    _this.doRefresh = function() {
      VacationService.get({
        vacationId: $stateParams.id
      }).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.vacation = res
        }
        $scope.$broadcast('scroll.refreshComplete')
      })
    }
    _this.$stateParams = $stateParams

    this.goVacationDelay = function(vacation) {
      if (!vacation.leaveDelayId) {
        return
      }

      $state.transitionTo('tab.vacations.delaydetail', {
          id: $stateParams.id,
          delay_id: vacation.leaveDelayId
        })
        // body...
    }
    _this.doRefresh()
  })
  .controller('VacationsListCtrl', function($scope, Utils, VacationService) {
    $scope.vacationsListCtrl = this
    var _this = this


    this.doRefresh = function() {
      VacationService.query().$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.vacations = res
        }
        $scope.$broadcast('scroll.refreshComplete')

      })
    }

    if (!Utils.isLogin()) {
      return
    }
    _this.doRefresh()
  })
  .controller('VacationsAddCtrl', function($scope, Utils, VacationService) {
    $scope.vacationsAddCtrl = this
    var _this = this

    this.isdate = true // 粒度到天
    this.isAdding = false
    this.startdate = new Date((parseInt((new Date()).getTime() / 3600 / 1000 / 24) * 24 - 8) * 3600 * 1000)
    this.enddate = new Date((parseInt((new Date()).getTime() / 3600 / 1000 / 24) * 24 - 8) * 3600 * 1000)
    this.starttime = ((new Date()).getHours() * 60 * 60)
    this.endtime = ((new Date()).getHours() * 60 * 60)
    this.timelist = []

    _this.srclist = []
    _this.datepicker = {
      titleLabel: '请选择日期', //Optional
      todayLabel: '今天', //Optional
      closeLabel: '关闭', //Optional
      setLabel: '选中', //Optional
      setButtonType: 'button-positive', //Optional
      todayButtonType: 'button-positive', //Optional
      closeButtonType: 'button-stable', //Optional
      inputDate: _this.startdate, //Optional
      mondayFirst: true, //Optional
      // disabledDates: disabledDates, //Optional
      weekDaysList: Utils.weekDaysListZh, //Optional
      monthList: Utils.monthListZh, //Optional
      templateType: 'modal', //Optional
      showTodayButton: 'true', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      from: new Date(), //Optional
      // to: new Date(2018, 8, 25),  //Optional
      callback: function(val) { //Mandatory
        if (!val) return
        _this.datepicker.inputDate = val
        _this.isdate ? 0 : angular.element(document.querySelector('#timepicker')).triggerHandler('click')
      },
      dateFormat: 'yyyy-MM-dd', //Optional
      closeOnSelect: false, //Optional
    }

    _this.timepicker = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60), //Optional
      step: 60, //Optional
      format: 24, //Optional
      titleLabel: '请选择时间', //Optional
      setLabel: '选中', //Optional
      closeLabel: '关闭', //Optional
      setButtonType: 'button-positive', //Optional
      closeButtonType: 'button-stable', //Optional
      callback: function(val) { //Mandatory
        if (!val) {
          return
        }
        _this.timepicker.inputEpochTime = val
      }
    }

    // 设置时间 type: start,end
    this.set = function(type) {
        angular.element(document.querySelector('#datepicker')).triggerHandler('click')
        this.datepicker.callback = function(val) {
          if (!val) {
            return
          }
          _this.datepicker.inputEpochTime = val
          type === 'start' ? _this.startdate = val : _this.enddate = val
          _this.isdate ? 0 : angular.element(document.querySelector('#timepicker')).triggerHandler('click')
        }

        this.timepicker.callback = function(val) {
          if (!val) {
            return
          }
          type === 'start' ? _this.starttime = val : _this.endtime = val
          _this.timepicker.inputEpochTime = val
        }
      }
      // console.log(this)
    this.addTime = function() {
      console.log(_this.startdate.toLocaleString())
      console.log(_this.isdate)
      if (_this.isdate) {
        _this.starttime = 0
        _this.endtime = 0
      }
      _this.timelist.push({
        startdate: _this.startdate,
        starttime: _this.starttime,
        enddate: _this.enddate,
        endtime: _this.endtime,
        start: _this.startdate.getTime() + _this.starttime * 1000,
        end: _this.enddate.getTime() + _this.endtime * 1000 + 24 * 3600 * 1000
      })
    }

    this.deleteTime = function(ele) {
      _.remove(_this.timelist, ele)
    }

    this.add = function() {
      // 拼出来leaveTimes
      _this.vacation.leaveTimes = []
      for (var i = 0; i < _this.timelist.length; i++) {
        var time = {
          startTime: _this.timelist[i]['start'],
          endTime: _this.timelist[i]['end']
        }
        _this.vacation.leaveTimes.push(time)
      }

      VacationService.save(_this.vacation).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          Utils.goBack()
        }
      })
    }

    _this.public = function() {

      if(_this.srclist.length == 0){
        _this.add()
        return
      }
      console.log("开始上传：")
      var wt = plus.nativeUI.showWaiting()
      var task = plus.uploader.createUpload(baseUrl + '/file', {
            method: "POST"
          },
          function(t, status) { //上传完成
            wt.close()
            console.log(status)
            console.log(angular.toJson(t))
            if (status == 200) {
              console.log("上传成功：" + t.responseText)
              var response = angular.fromJson(t.responseText)
              console.log(response)

              if (Utils.isResponseOk(response)) {
                console.log(response)
                if(_this.srclist.length <= 1){
                  if (response[0]) _this.vacation.imgurl1 = response[0]
                }
                if(_this.srclist.length <= 2){
                  if (response[1]) _this.vacation.imgurl2 = response[1]
                }
                if(_this.srclist.length >= 3){
                  if (response[2]) _this.vacation.imgurl3 = response[2]
                }

                _this.add()
              } else {
                $rootScope.toast('提交失败')
              }
            }
          }

        )
        // console.log(content)
        // task.addData("content", content)
      for (var i = 0; i < _this.srclist.length; i++) {
        var f = _this.srclist[i]
        var index = i == 0? '' : i
        task.addFile(f, {
          key: 'file' + index
        })
      }

      task.start()
      console.log(angular.toJson(task))
    }

    _this.showActionSheet = function() {
      if (_this.srclist.length >= 3) {
        return
      }
      //  	outSet( "弹出系统选择按钮框：" );
      var bts = [{
        title: "拍照上传"
      }, {
        title: "相册中选择"
      }]
      plus.nativeUI.actionSheet({
          title: "请选择",
          cancel: "取消",
          buttons: bts
        },

        function(e) {
          switch (e.index) {
            case 1:
              // 拍照上传
              console.log("开始拍照：");
              var cmr = plus.camera.getCamera();
              cmr.captureImage(function(p) {
                console.log("成功：" + p);
                plus.io.resolveLocalFileSystemURL(p, function(entry) {
                  _this.srclist.push(entry.toLocalURL());
                }, function(e) {
                  console.log("读取拍照文件错误：" + e.message);
                });
              }, function(e) {
                console.log("失败：" + e.message);
              }, {
                filename: "_doc/camera/",
                index: 1
              })
              break
            case 2:
              // 系统相册
              plus.gallery.pick(function(path) {
                plus.io.resolveLocalFileSystemURL(path, function(entry) {
                  _this.srclist.push(entry.toLocalURL())
                }, function(e) {
                  console.log("读取拍照文件错误：" + e.message);
                })
              }, function(e) {
                console.log("取消选择图片")
              }, {
                filter: "image"
              })
              break;
          }
        }
      );
    }
  })
  .controller('VacationsAddDelayDetailCtrl', function($scope, $stateParams, $rootScope, Utils, VacationService, StatusUtil) {
    $scope.vacationsAddDelayDetailCtrl = this
    var _this = this
    VacationService.get({
      vacationId: $stateParams.id
    }).$promise.then(function(res) {
      if (Utils.isResponseOk(res)) {
        _this.vacation = res

        _this.firstType = angular.copy($rootScope.findFirstType(res.vacationInfo.type))
      }
    })

    this.isdate = true
    this.isAdding = false
    this.startdate = new Date((parseInt((new Date()).getTime() / 3600 / 1000 / 24) * 24 - 8) * 3600 * 1000)
    this.enddate = new Date((parseInt((new Date()).getTime() / 3600 / 1000 / 24) * 24 - 8) * 3600 * 1000)
    console.log(this.startdate.toLocaleString())
    this.starttime = ((new Date()).getHours() * 60 * 60)
    this.endtime = ((new Date()).getHours() * 60 * 60)
    this.timelist = []

    this.datepicker = {
      titleLabel: '请选择日期', //Optional
      todayLabel: '今天', //Optional
      closeLabel: '关闭', //Optional
      setLabel: '选中', //Optional
      setButtonType: 'button-positive', //Optional
      todayButtonType: 'button-positive', //Optional
      closeButtonType: 'button-stable', //Optional
      inputDate: _this.startdate, //Optional
      mondayFirst: true, //Optional
      // disabledDates: disabledDates, //Optional
      weekDaysList: Utils.weekDaysListZh, //Optional
      monthList: Utils.monthListZh, //Optional
      templateType: 'modal', //Optional
      showTodayButton: 'true', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      from: new Date(), //Optional
      // to: new Date(2018, 8, 25),  //Optional
      callback: function(val) { //Mandatory
        if (!val) return
        _this.datepicker.inputDate = val
        _this.isdate ? 0 : angular.element(document.querySelector('#timepicker')).triggerHandler('click')
      },
      dateFormat: 'yyyy-MM-dd', //Optional
      closeOnSelect: false, //Optional
    }

    _this.timepicker = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60), //Optional
      step: 30, //Optional
      format: 24, //Optional
      titleLabel: '请选择时间', //Optional
      setLabel: '选中', //Optional
      closeLabel: '关闭', //Optional
      setButtonType: 'button-positive', //Optional
      closeButtonType: 'button-stable', //Optional
      callback: function(val) { //Mandatory
        if (!val) {
          return
        }
        _this.timepicker.inputEpochTime = val
      }
    }

    // 设置时间 type: start,end
    this.set = function(type) {
        angular.element(document.querySelector('#datepicker')).triggerHandler('click')
        _this.datepicker.callback = function(val) {
          if (!val) {
            return
          }
          _this.datepicker.inputEpochTime = val
          type === 'start' ? _this.startdate = val : _this.enddate = val
          _this.isdate ? 0 : angular.element(document.querySelector('#timepicker')).triggerHandler('click')
        }

        _this.timepicker.callback = function(val) {
          if (!val) {
            return
          }
          type === 'start' ? _this.starttime = val : _this.endtime = val
          _this.timepicker.inputEpochTime = val
          time = val
        }
      }
      // console.log(this)
    this.addTime = function() {
      if (_this.isdate) {
        _this.starttime = 0
        _this.endtime = 0
      }
      _this.timelist.push({
        startdate: _this.startdate,
        starttime: _this.starttime,
        enddate: _this.enddate,
        endtime: _this.endtime,
        start: _this.startdate.getTime() + _this.starttime * 1000,
        end: _this.enddate.getTime() + _this.endtime * 1000 + 24 * 3600 * 1000
      })
    }

    this.deleteTime = function(ele) {
      _.remove(_this.timelist, ele)
    }

    _this.srclist = []
    this.add = function() {
      // 拼出来个字符串 start, end;start,end......
      // _this.vacationDelay.leaveTime = ''
      // for (var i = 0; i < _this.timelist; i++) {
      //   if (i != 0) {
      //     _this.vacationDelay.leaveTime += ';'
      //   }
      //   var time = _this.timelist[i]
      //   _this.vacationDelay.leaveTime += time.start + ',' + time.end
      // }
      // 拼出来个leavetimes :start_time,end_time
      // _this.vacationDelay.leaveTimes = []
      for (var i = 0; i < _this.timelist.length; i++) {
        _this.vacationDelay.delayStarttime = _this.timelist[i].start
        _this.vacationDelay.delayEndtime = _this.timelist[i].end
          // console.log(_this.vacationDelay)
      }
      _this.vacationDelay.type = ''

      // 拼接子类型
      for (var i = 0; i < _this.firstType.subTypes.length; i++) {
        var type = _this.firstType.subTypes[i]
        if (type.isCheck) {
          if (_this.vacationDelay.type.length != 0) {
            _this.vacationDelay.type += ','
          }
          _this.vacationDelay.type += type.id
        }
      }

      VacationService.applyDelay({
        'vacationId': $stateParams.id
      }, _this.vacationDelay).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          Utils.goBack()
        }
      })
    }

    _this.public = function() {
      if(_this.srclist.length == 0){
        _this.add()
        return
      }
      console.log("开始上传：")
      var wt = plus.nativeUI.showWaiting()
      var task = plus.uploader.createUpload(baseUrl + '/file', {
            method: "POST"
          },
          function(t, status) { //上传完成
            wt.close()
            console.log(status)
            console.log(angular.toJson(t))
            if (status == 200) {
              console.log("上传成功：" + t.responseText)
              var response = angular.fromJson(t.responseText)
              console.log(response)

              if (Utils.isResponseOk(response)) {
                console.log(response)
                if(_this.srclist.length <= 1){
                  if (response[0]) _this.vacationDelay.imgurl1 = response[0]
                }
                if(_this.srclist.length <= 2){
                  if (response[1]) _this.vacationDelay.imgurl2 = response[1]
                }
                if(_this.srclist.length >= 3){
                  if (response[2]) _this.vacationDelay.imgurl3 = response[2]
                }

                _this.add()
              } else {
                $rootScope.toast('提交失败')
              }
            }
          }

        )
        // console.log(content)
        // task.addData("content", content)
      for (var i = 0; i < _this.srclist.length; i++) {
        var f = _this.srclist[i]
        var index = i == 0? '' : i
        task.addFile(f, {
          key: 'file' + index
        })
      }

      task.start()
      console.log(angular.toJson(task))
    }

    _this.showActionSheet = function() {
      if (_this.srclist.length >= 3) {
        return
      }
      //  	outSet( "弹出系统选择按钮框：" );
      var bts = [{
        title: "拍照上传"
      }, {
        title: "相册中选择"
      }]
      plus.nativeUI.actionSheet({
          title: "请选择",
          cancel: "取消",
          buttons: bts
        },

        function(e) {
          switch (e.index) {
            case 1:
              // 拍照上传
              console.log("开始拍照：")
              var cmr = plus.camera.getCamera()
              cmr.captureImage(function(p) {
                console.log("成功：" + p)
                plus.io.resolveLocalFileSystemURL(p, function(entry) {
                  _this.srclist.push(entry.toLocalURL())
                }, function(e) {
                  console.log("读取拍照文件错误：" + e.message)
                })
              }, function(e) {
                console.log("失败：" + e.message)
              }, {
                filename: "_doc/camera/",
                index: 1
              })
              break
            case 2:
              // 系统相册
              plus.gallery.pick(function(path) {
                plus.io.resolveLocalFileSystemURL(path, function(entry) {
                  _this.srclist.push(entry.toLocalURL())
                }, function(e) {
                  console.log("读取拍照文件错误：" + e.message)
                })
              }, function(e) {
                console.log("取消选择图片")
              }, {
                filter: "image"
              })
              break;
          }
        }
      );
    }
  })
  .controller('VacationsDelayDetailCtrl', function($scope, $stateParams, $rootScope, Utils, VacationService) {
    $scope.vacationsDelayDetailCtrl = this
    var _this = this

    _this.doRefresh = function() {
      VacationService.get({
        vacationId: $stateParams.id
      }).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.vacation = res
          _this.setSubType()

        }
      })
      VacationService.getDelayDetail({
        vacationId: $stateParams.id,
        r1Id: $stateParams.delay_id
      }).$promise.then(function(res) {
        if (Utils.isResponseOk(res)) {
          _this.vacationDelay = res
          _this.setSubType()
        }
        $scope.$broadcast('scroll.refreshComplete')

      })
    }

    _this.setSubType = function() {
      if (!_this.vacation || !_this.vacationDelay) return
      _this.subtypelist = $rootScope.generateSubTypes(_this.vacation.vacationInfo.type, _this.vacationDelay.type)
        // console.log(_this.subtypelist)
    }
    _this.doRefresh()
  })
  // .controller('VacationsDelayDetailCtrl', function(this) {})
