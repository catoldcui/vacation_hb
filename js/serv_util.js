angular.module('starter.services', [])

.factory('Utils', function($cookies, $rootScope) {
  var _this = this
  this.debug = true

  this.monthListZh = ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
  this.weekDaysListZh = ["日", "一", "二", "三", "四", "五", "六"]

  this.user = {}

  this.setUser = function(user){
    $cookies.putObject('user', user)
    $rootScope.user = user
  }

  this.getUser = function(){
    return $cookies.getObject('user')
  }

  this.removeUser = function(){
    delete $rootScope.user
    $cookies.remove('user')
  }

  this.isLogin = function(){
    return $cookies.getObject('user')
  }

  this.goBack = function(){
    $rootScope.goBack()
  }
  // 判断返回值事不是状态码,而且状态码不是200
  this.isResponseOk = function (res) {
    return !(res.state != undefined && res.message != undefined && res.state != 200)
  }

  // 判断是否有审批的权力
  this.canJudge = function(){
    // console.log(1111111)
    return _this.isLogin() && (
      _this.getUser().role.indexOf('general_manager') >= 0 ||
      _this.getUser().role.indexOf('statistic_user') >= 0 ||
      _this.getUser().role.indexOf('deputy_manager') >= 0)
  }

  this.isStatistic = function () {
    return _this.isLogin() && _this.getUser().role.indexOf('statistic_user') >= 0
  }

  this.isUserCanJudgeVacation = function (vacation) {
    if(!vacation) return false
    return _this.isLogin() && (
      (_this.isStatistic() && vacation.status == 'FIRST_PASSED') ||
      (!_this.isStatistic() && vacation.status == 'WAITING_FOR_FIRST_APPLY')
    )
  }

  // 判断是否有统计的权力
  this.canCalculate = function () {
    return _this.isLogin() && (_this.getUser().role.indexOf('statistic_user') >= 0 ||
      _this.getUser().role.indexOf('general_manager') >= 0 ||
      _this.getUser().role.indexOf('deputy_manager') >= 0)
  }
  return this
})
