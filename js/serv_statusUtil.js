angular.module('starter.services')

.factory('StatusUtil', function($rootScope) {
  var _this = this
    // WAITING_FOR_FIRST_APPLY("等待审批"),
    // FIRST_PASSED("一级审批通过", VacationStatus.WAITING_FOR_FIRST_APPLY),
    // FIRST_BLOCKED("一级审批驳回", VacationStatus.WAITING_FOR_FIRST_APPLY),
    // SECOND_PASSED("二级审批通过", VacationStatus.FIRST_PASSED),
    // SECOND_BLOCKED("二级审批驳回", VacationStatus.FIRST_PASSED),
    // WAITING_FOR_DELAY_APPLY("延期等待审批", VacationStatus.SECOND_PASSED, VacationStatus.DELAY_BLOCKED, VacationStatus.DELAY_PASSED),
    // DELAY_BLOCKED("延期未通过", VacationStatus.WAITING_FOR_DELAY_APPLY),
    // DELAY_PASSED("延期通过", VacationStatus.WAITING_FOR_FIRST_APPLY)

  _this.allstatus = {
    'WAITING_FOR_FIRST_APPLY': {
      name: '等待审批',
      prestatus: []
    },
    'FIRST_PASSED': {
      name: '一级审批通过',
      prestatus: ['WAITING_FOR_FIRST_APPLY']
    },
    'FIRST_BLOCKED': {
      name: '一级审批驳回',
      prestatus: ['WAITING_FOR_FIRST_APPLY']
    },
    'SECOND_PASSED': {
      name: '二级审批通过',
      prestatus: ['FIRST_PASSED']
    },
    'SECOND_BLOCKED': {
      name: '二级审批驳回',
      prestatus: ['FIRST_PASSED']
    },
    'WAITING_FOR_DELAY_APPLY': {
      name: '延期等待审批',
      prestatus: ['SECOND_PASSED', 'DELAY_BLOCKED', 'DELAY_PASSED']
    },
    'DELAY_BLOCKED': {
      name: '延期未通过',
      prestatus: ['WAITING_FOR_DELAY_APPLY']
    },
    'DELAY_PASSED': {
      name: '延期通过',
      prestatus: ['WAITING_FOR_DELAY_APPLY']
    },
  }

  // 判断该状态是否可申请延期
  _this.allowAddDealy = function(vacation){
    if(!vacation) return false
    var subTypes = $rootScope.findFirstType(vacation.type).subTypes
    return _this.allstatus['WAITING_FOR_DELAY_APPLY'].prestatus.indexOf(vacation.status) >= 0 && subTypes && subTypes.length > 0
    // _.find(_this.allstatus['WAITING_FOR_DELAY_APPLY'], status)
  }

  return this
})
