angular.module('starter.services')
.factory('VacationTypeService', function ($resource) {
  return $resource(baseUrl + '/vacation_type/:type1_key/:r1', {
    'type1_key' : '@id'
  }, {
    getDelayType: {
      method: 'GET',
      params: {
        'r1' : 'type2'
      }
    }
  })
})
